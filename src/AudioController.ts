export const DEFAULT_MASTER_VOLUME = 0.5;
export const DEFAULT_SUPPRESSION_VOLUME = 0.3;
export const DEFAULT_AUDIO_VOLUME_TRANSITION = 0.5; // seconds

export interface AudioState {
	_arrayBuffer?: ArrayBuffer;
	_audioBuffer?: AudioBuffer;
	src: string;
	loaded: boolean;
	onLoad?: () => void;
	currentSource?: AudioBufferSourceNode;
	gainNode?: GainNode;
	initialVolume: number;
	currentVolume: number;
	tags: Set<string>;
	loop?: {
		start: number;
		end?: number;
	};
	transitionDuration: number;
	transitionTimer?: number;
}

export interface PlaySettings {
	when?: number;
	volume?: number | 'initial';
	suppressionOther?: boolean;
	dontUseTransition?: boolean;
}

export class AudioController {
	private readonly audiosStates: { [key: string]: AudioState } = {};
	private readonly suppressors: Set<string> = new Set<string>();
	private readonly pending: ((context: AudioContext) => void)[] = [];
	private readonly suppressionVolume: number = DEFAULT_SUPPRESSION_VOLUME;
	private readonly audioVolumeTransition: number = DEFAULT_AUDIO_VOLUME_TRANSITION;
	private masterVolume: number = DEFAULT_MASTER_VOLUME;
	private context: AudioContext | null = null;

	constructor(options?: { audioVolumeTransition?: number; suppressionVolume?: number; masterVolume?: number }) {
		if (options?.audioVolumeTransition !== undefined) {
			this.audioVolumeTransition = options.audioVolumeTransition;
		}
		if (options?.suppressionVolume !== undefined) {
			this.suppressionVolume = options.suppressionVolume;
		}
		if (options?.masterVolume !== undefined) {
			this.changeMasterVolume(options.masterVolume);
		}
	}

	public changeMasterVolume(volume: number): void {
		// fix on expected behaviour with sum of float numbers like 0.1 + 0.2 that gives non-rounded answer (0.30000000000000004)
		this.masterVolume = volume;

		if (this.context === null) {
			return;
		}

		for (const [src, audioState] of Object.entries(this.audiosStates)) {
			void this.changeSettings(src, {
				volume: audioState.currentVolume,
				dontUseTransition: true,
			});
		}
	}

	public async init(): Promise<AudioController> {
		const webkitAudioContext = ((window as unknown) as { webkitAudioContext: (new () => AudioContext) | undefined })
			.webkitAudioContext;
		const context = (this.context =
			typeof webkitAudioContext !== 'undefined' ? new webkitAudioContext() : new AudioContext());

		this.pending.forEach((callback) => callback(context));

		return this;
	}

	public add(
		sources: string | string[],
		defaultOptions: {
			initialVolume?: number;
			tags?: string[];
			loop?: {
				start: number;
				end?: number;
			};
			transitionDuration?: number;
		} = {},
		callback?: () => void,
	): AudioController & {
		toPromises: () => Promise<void>[];
	} {
		if (!(sources instanceof Array)) {
			sources = [sources];
		}

		const promises: Promise<void>[] = [];
		let loaded = 0;
		for (const src of sources) {
			const initialVolume = defaultOptions.initialVolume ?? 1;
			this.audiosStates[src] = {
				src,
				loaded: false,
				initialVolume,
				currentVolume: initialVolume,
				tags: new Set(defaultOptions.tags ?? []),
				loop: defaultOptions.loop,
				transitionDuration: defaultOptions.transitionDuration ?? this.audioVolumeTransition,
			};
			const audioState = this.audiosStates[src];

			promises.push(
				this.load(src).then((arrayBuffer) => {
					audioState._arrayBuffer = arrayBuffer;

					void this.decodeAudioData(audioState).then(() => {
						audioState.onLoad?.();
						audioState.loaded = true;

						if (++loaded === sources.length) {
							callback?.();
						}
					});
				}),
			);
		}

		const proxyAdd = (
			...args: Parameters<typeof AudioController.prototype.add>
		): ReturnType<typeof AudioController.prototype.add> => {
			const result = this.add(...args);
			promises.push(...result.toPromises());

			// @ts-ignore
			return {
				...result,
				add: proxyAdd,
				toPromises: () => promises,
			};
		};

		return {
			...this,
			add: proxyAdd,
			toPromises: () => promises,
		};
	}

	public async play(src: string, settings: PlaySettings = {}): Promise<void> {
		const audioState = await this.getAudioState(src, true);
		const { currentSource } = audioState;

		await this.changeSettings(src, {
			volume: audioState.currentVolume,
			dontUseTransition: true,
			...settings,
		});
		currentSource.start(settings.when ?? 0);

		if (settings.suppressionOther !== undefined) {
			this.suppressors.add(src);

			currentSource.onended = () => {
				if (this.suppressors.has(src)) {
					this.removeSuppressor(src);
				}
			};

			Object.values(this.audiosStates).forEach((audioStateValue) => {
				if (audioStateValue.gainNode !== undefined) {
					audioStateValue.gainNode.gain.value = this.suppressionVolume;
				}
			});
		}
	}

	public async stop(src: string): Promise<void> {
		const audioState = await this.getAudioState(src);
		try {
			audioState.currentSource.stop();
			// eslint-disable-next-line no-empty
		} catch (err) {}

		if (this.suppressors.has(src)) {
			this.removeSuppressor(src);
		}
	}

	public async stopByTag(tag: string): Promise<void> {
		for (const [src, audioState] of Object.entries(this.audiosStates)) {
			if (audioState.tags.has(tag)) {
				await this.stop(src);
			}
		}
	}

	public async stopAll(): Promise<void> {
		for (const src of Object.keys(this.audiosStates)) {
			await this.stop(src);
		}
	}

	public async changeSettings(
		src: string,
		settings: {
			dontUseTransition?: boolean;
			volume?: number | 'initial';
		},
	): Promise<void> {
		const { volume, dontUseTransition } = settings;

		const audioState = await this.getAudioState(src);
		const context = this.context!;

		if (volume !== undefined) {
			const { gainNode } = audioState;
			const newVolume = volume === 'initial' ? audioState.initialVolume : volume;
			audioState.currentVolume = newVolume;

			if (audioState.transitionTimer !== undefined) {
				gainNode.gain.cancelScheduledValues(context.currentTime);
				clearTimeout(audioState.transitionTimer);
			}

			if (dontUseTransition === true) {
				gainNode.gain.value = newVolume * this.masterVolume;
				return;
			}

			let currentVolume = gainNode.gain.value;
			if (currentVolume === 0) {
				currentVolume = 0.01;
			}
			let expectedVolume = newVolume * this.masterVolume;
			if (expectedVolume === 0) {
				expectedVolume = 0.01;
			}

			gainNode.gain.setValueAtTime(currentVolume, context.currentTime);
			gainNode.gain.exponentialRampToValueAtTime(expectedVolume, context.currentTime + audioState.transitionDuration);

			audioState.transitionTimer = window.setTimeout(() => {
				gainNode.gain.value = newVolume * this.masterVolume;
			}, audioState.transitionDuration * 1000);
		}
	}

	private getAudioState(
		src: string,
		createNewBuffer: boolean = false,
	): Promise<
		AudioState & {
			_audioBuffer: AudioBuffer;
			currentSource: AudioBufferSourceNode;
			gainNode: GainNode;
		}
	> {
		return new Promise((res, rej) => {
			const callback = (context: AudioContext): void => {
				const audioState = this.audiosStates[src];
				if (audioState == null) {
					rej(`audioState with src: "${src}" has not been created`);
					return;
				}

				const loadCallback = (): void => {
					const { _audioBuffer, loop } = audioState;
					if (_audioBuffer === undefined) {
						rej(`unexpected error: audioBuffer is unavailable for sound: "${src}"`);
						return;
					}

					if (createNewBuffer || audioState.currentSource === undefined || audioState.gainNode === undefined) {
						let gainNode: GainNode | undefined = audioState.gainNode;
						if (gainNode === undefined) {
							gainNode = context.createGain();
							gainNode.connect(context.destination);

							audioState.gainNode = gainNode;
						}

						const source = context.createBufferSource();
						source.buffer = _audioBuffer;
						source.connect(gainNode);

						audioState.currentSource = source;

						if (loop !== undefined) {
							source.loop = true;
							source.loopStart = loop.start;

							if (loop.end !== undefined) {
								source.loopEnd = loop.end;
							}
						}
					}

					// @ts-ignore
					res(audioState);
				};

				if (audioState.loaded) {
					loadCallback();
				} else {
					audioState.onLoad = loadCallback;
				}
			};

			if (this.context === null) {
				this.pending.push(callback);
			} else {
				callback(this.context);
			}
		});
	}

	private async load(src: string): Promise<ArrayBuffer> {
		return new Promise((res, rej) => {
			const request = new XMLHttpRequest();
			request.open('GET', src, true);
			request.responseType = 'arraybuffer';

			request.onload = () => {
				res(request.response);
			};

			request.onerror = function () {
				rej('BufferLoader: XHR error');
			};

			request.send();
		});
	}

	private removeSuppressor(src: string): void {
		this.suppressors.delete(src);
		if (this.suppressors.size === 0) {
			this.unsuppressAll();
		}
	}

	private unsuppressAll(): void {
		for (const { gainNode, currentVolume } of Object.values(this.audiosStates)) {
			if (gainNode !== undefined) {
				gainNode.gain.value = currentVolume;
			}
		}
	}

	private async decodeAudioData(audioState: AudioState): Promise<void> {
		return new Promise((res, rej) => {
			const callback = (): void => {
				void this.context!.decodeAudioData(
					audioState._arrayBuffer!,
					(buffer) => {
						if (buffer == null) {
							rej(`error decoding file data: ${audioState.src}`);
							return;
						}

						audioState._audioBuffer = buffer;
						res(undefined);
					},
					function (error) {
						console.error('decodeAudioData error', error);
						res(undefined);
					},
				);
			};

			if (this.context === null) {
				this.pending.push(callback);
			} else {
				callback();
			}
		});
	}
}
