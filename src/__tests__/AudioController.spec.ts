import { AudioController } from '../AudioController';

describe('AudioController', () => {
	let audioController: AudioController;

	beforeAll(() => {
		audioController = new AudioController();
	});

	describe('toPromise method as return result of add-method', () => {
		it('.add chains all promises and returns them in method', () => {
			const promises = audioController.add(['first', 'second']).add('third').add(['fourth']).toPromises();
			expect(promises).toHaveLength(4);
		});
	});
});
